.   �   @class MenuBuilder 
 @brief Create Dynamic Context and MenuBar Menus


 Summary : The MenuBuilder script class exists merely as a helper for creating
           popup menu's for use in torque editors.  It is setup as a single 
           object with dynamic fields starting with item[0]..[n] that describe
           how to create the menu in question.  An example is below.

 isPopup : isPopup is a persistent field on PopupMenu console class which
           when specified to true will allow you to perform .showPopup(x,y) 
           commands which allow popupmenu's to be used/reused as menubar menus
           as well as context menus.

 barPosition : barPosition indicates which index on the menu bar (0 = leftmost)
           to place this menu if it is attached.  Use the attachToMenuBar() command
           to attach a menu.

 barName : barName specifies the visible name of a menu item that is attached
           to the global menubar.

 canvas  : The GuiCanvas object the menu should be attached to. This defaults to
           the global Canvas object if unspecified.

 Remarks : If you wish to use a menu as a context popup menu, isPopup must be 
           specified as true at the creation time of the menu.

 
 @li @b item[n] (String) TAB (String) TAB (String) : <c>A Menu Item Definition.</c>
 @code item[0] = "Open File..." TAB "Ctrl O" TAB "Something::OpenFile"; @endcode

 @li @b isPopup (bool) : <c>If Specified the menu will be considered a popup menu and should be used via .showPopup()</c>
 @code isPopup = true; @endcode


 Example : Creating a @b MenuBar Menu
 @code
 %%editMenu = new PopupMenu()
 {
    barPosition = 3;
    barName     = "View";
    superClass = "MenuBuilder";
    item[0] = "Undo" TAB "Ctrl Z" TAB "levelBuilderUndo(1);";
    item[1] = "Redo" TAB "Ctrl Y" TAB "levelBuilderRedo(1);";
    item[2] = "-";
 };

 %%editMenu.attachToMenuBar( 1, "Edit" );

 @endcode


 Example : Creating a @b Context (Popup) Menu
 @code
 %%contextMenu = new PopupMenu()
 {
    superClass = MenuBuilder;
    isPopup    = true;
    item[0] = "My Super Cool Item" TAB "Ctrl 2" TAB "echo(\"Clicked Super Cool Item\");";
    item[1] = "-";
 };

 %%contextMenu.showPopup();
 @endcode


 Example : Modifying a Menu
 @code
 %%editMenu = new PopupMenu()
 {
    item[0] = "Foo" TAB "Ctrl F" TAB "echo(\"clicked Foo\")";
    item[1] = "-";
 };
 %%editMenu.addItem( 2, "Bar" TAB "Ctrl B" TAB "echo(\"clicked Bar\")" );
 %%editMenu.removeItem( 0 );
 %%editMenu.addItem( 0, "Modified Foo" TAB "Ctrl F" TAB "echo(\"clicked modified Foo\")" );
 @endcode


 @see PopupMenu

 addItem MenuBuilder %item Item %pos %this getField %name %accel %cmd strreplace setField isObject insertSubMenu insertItem appendItem getItemCount onAdd Canvas %i onRemove removeFromMenuBar onSelectItem %id eval  Sets a new name on an existing menu item.
 setItemName setItem  Sets a new command on an existing menu item.
 setItemCommand %command  (SimID this)
 Wraps the attachToMenuBar call so that it does not require knowledge of
 barName or barIndex to be removed/attached.  This makes the individual 
 MenuBuilder items very easy to add and remove dynamically from a bar.

 attachToMenuBar barName error barPosition Parent onAttachToMenuBar onRemoveFromMenuBar  Method called to setup default state for the menu. Expected to be overriden
 on an individual menu basis. See the mission editor for an example.
 setupDefaultState  Method called to easily enable or disable all items in a menu.
 enableAllItems enableItem %enable �    0 1 2 [this] - MenuBuilder::attachToMenuBar - Menu property 'barName' not specified. MenuBuilder::attachToMenuBar - Menu  property 'barPosition' is invalid, must be zero or greater.              �?        �  <   G     ��   $ *NF Q&$ *K$ *.1 P26% -<$ *N$ *K$ *.1 P26QN$ *K$ *K$ *.1 P2P9<S$ *RFRI   % -<S$ *RFRI   % -<S$ *RFRI   % -<S$ *RFR$ *RI   % -<S$ *RFR$ *RI   K$ *K$ *.1 P2P9<S$ *RI   ;�S$ *R$ *R$ *R$ *RJ  <�S$ *R$ *R$ *NFQ�$ *�F R$ *RJ  <    ��    S$ *RS$ *RJ  R$ *RJ  <    ��   ��   S$ *.1 6RI   ;�B  H K$ *.1 P9<C % +B$ *K$ *.1 P26NF Q��  S$ *R$ *RJ  <D % ),?$ *K$ *.1 P26NF Q�\      �  ��   S$ *RJ  <    �#  ��     S$ *K$ *.1 P26RFRI   % -<$ *NF Q��  S$ *RI   <FFG��
      �;  �     $ *K$ *.1 P26% -<S$ *RFRI   % -<S$ *R$ *R$ *R$ *RJ  <G�/      �K  �T     S$ *K$ *.1 P26RFR$ *RI   K$ *K$ *.1 P2P9<G�v      �_  ��   $ *.1 6NF Q�w  SFRI   <FD$ *.1 5��  SFVL $ *.1 6OL F{ORI   <FS$ *R$ *.1 6R$ *.1 6R$ *.1 6RJ  <    ��  ��          ��  ��    G��      ��  �|   C % +B$ *K$ *.1 P26NF Q�{  S$ *K$ *.1 P26RFRI   % -<S$ *K$ *.1 P26RFRI   % -<S$ *K$ *.1 P26RFRI   % -<S$ *RI   ;�_  S$ *RJ  <D % ),?$ *K$ *.1 P26NF Q��  G�[      ��  ��    C % +B$ *K$ *.1 P26NF Q��  S$ *R$ *R$ *RJ  <D % ),?$ *K$ *.1 P26NF Q��  	       a      b      d   &   e   ;   g   N   h   ^   i   n   l   ~   m   �   o   �   r   �   v   �   _   �   |     z     �   %  �   6  �   B  �   \     �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �     �     �     �   )  �   S  �   T  �   ^  �   k  �   t  �   w  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �   �  �     �   /  �   I  �   U  �   {  �   |  �   �  �   �  �   �  +   �     �  �     �  �  �       I     �  [  �     �  �     �  �     �  �  �     �  �     �  }     ~  �  w     p  �  o     c  �  �  _     W  �  m     (  <  ^        '            �  �
     �  �     �  �
     �  E  +  '    �  �  �  �
     �  �
     �  �
     �  �
     E  �  �  �  �  �  h  b  1    �  �  �  s  m  b  I  �
     +  �  >  7  �
       �
       �
     �   �
     �   }
     �   t
     �   O  /  k
     �   @  `
     �   [
     {   F  �  �  �  �   �   �   T
  	   k   W  K  ,      �   �   �   N
     [       �  �   �   �   E
     W   B  (    �  �  w   g   5
        �  �  p  9      �  M  3  �  �  {  Q  �   H   3   /
        �  �      �   p   `   P   <   '   #      :
     
   �  �   �   �   @   +      ?
  /   	   �  �  �  �  l  5      �  �  �  �  �  �  �  �  �  z  _  ]  I  /  &    �  �  �  �  �  �  w  ^  M  :  '  $        �   �   �   �   D   /      #
        �  �  �  �  X  !  �  �  �    �   
        f    