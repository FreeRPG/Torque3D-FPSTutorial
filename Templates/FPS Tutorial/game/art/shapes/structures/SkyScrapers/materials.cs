//-----------------------------------------------------------------------------
// Copyright (c) 2012 GarageGames, LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

//--- SkyScraper_01.dae MATERIALS BEGIN ---

//--- SkyScraper_01.dae MATERIALS END ---


singleton Material(SkyScraper_01_rooftiles_SkyScraper_01)
{
   mapTo = "rooftiles.SkyScraper_01";
   specular[0] = "1 1 1 0";
   specularPower[0] = "1";
   translucentBlendOp = "None";
   diffuseMap[0] = "art/textures/structures/SkyScrapers/skyr39L.jpg";
   normalMap[0] = "";
   pixelSpecular[0] = "1";
   specularMap[0] = "";
   castShadows = "0";
   materialTag0 = "Chinatown";
   materialTag1 = "Unused?";
};
//--- SkyScraper_05.dae MATERIALS BEGIN ---
singleton Material(SkyScraper_05_rooftiles_SkyScraper_2)
{
	mapTo = "rooftiles.SkyScraper_2";

	diffuseMap[0] = "art/textures/structures/SkyScrapers/skyr30L.jpg";
	normalMap[0] = "";
	specularMap[0] = "";

	diffuseColor[0] = "1 1 1 0";
	specular[0] = "1 1 1 0";
	specularPower[0] = 1;

	doubleSided = false;
	translucent = false;
	translucentBlendOp = "None";
   pixelSpecular[0] = "1";
   castShadows = "0";
   materialTag0 = "Chinatown";
   materialTag1 = "Unused?";
};

//--- SkyScraper_05.dae MATERIALS END ---

//--- SkyScraper_07.dae MATERIALS BEGIN ---


//--- SkyScraper_07.dae MATERIALS END ---

//--- SkyScraper_09.dae MATERIALS BEGIN ---
singleton Material(SkyScraper_09_rooftiles_SkyScraper_4)
{
	mapTo = "rooftiles.SkyScraper_4";

	diffuseMap[0] = "art/textures/structures/SkyScrapers/skyr23L.jpg";
	normalMap[0] = "";
	specularMap[0] = "";

	diffuseColor[0] = "1 1 1 0";
	specular[0] = "1 1 1 0";
	specularPower[0] = 50;

	doubleSided = false;
	translucent = false;
	translucentBlendOp = "None";
   castShadows = "0";
   materialTag0 = "Chinatown";
   materialTag1 = "Unused?";
};

//--- SkyScraper_09.dae MATERIALS END ---

//--- SkyScraper_010.dae MATERIALS BEGIN ---
singleton Material(SkyScraper_010_rooftiles_SkyScraper_5)
{
	mapTo = "rooftiles.SkyScraper_5";

	diffuseMap[0] = "art/shapes/structures/SkyScrapers/skyr50L.jpg";
	normalMap[0] = "";
	specularMap[0] = "";

	diffuseColor[0] = "1 1 1 0";
	specular[0] = "1 1 1 0";
	specularPower[0] = 50;

	doubleSided = false;
	translucent = false;
	translucentBlendOp = "None";
	materialTag0 = "Chinatown";
	materialTag1 = "Unused?";
};

//--- SkyScraper_010.dae MATERIALS END ---

//--- SkyScraper_08.dae MATERIALS BEGIN ---
singleton Material(SkyScraper_08_rooftiles_SkyScraper_3)
{
	mapTo = "rooftiles.SkyScraper_3";

	diffuseMap[0] = "art/textures/structures/SkyScrapers/skyr34L.jpg";
	normalMap[0] = "";
	specularMap[0] = "";

	diffuseColor[0] = "1 1 1 0";
	specular[0] = "1 1 1 0";
	specularPower[0] = 50;

	doubleSided = false;
	translucent = false;
	translucentBlendOp = "None";
   castShadows = "0";
   materialTag0 = "Chinatown";
   materialTag1 = "Unused?";
};

//--- SkyScraper_08.dae MATERIALS END ---

//--- SkyS_01.dae MATERIALS BEGIN ---

//--- SkyS_01.dae MATERIALS END ---


//--- SkyS_02.dae MATERIALS BEGIN ---
singleton Material(SkyS_02_rooftiles_SkyS02)
{
	mapTo = "unmapped_mat";

	diffuseMap[0] = "art/textures/structures/SkyScrapers/SkyS_02.dds";
	normalMap[0] = "";
	specularMap[0] = "";

	diffuseColor[0] = "1 1 1 0";
	specular[0] = "1 1 1 0";
	specularPower[0] = 50;

	doubleSided = false;
	translucent = false;
	translucentBlendOp = "None";
	materialTag0 = "Chinatown";
	materialTag1 = "Unused?";
};

//--- SkyS_02.dae MATERIALS END ---

//--- SkyS_03.dae MATERIALS BEGIN ---
singleton Material(SkyS_03_rooftiles_SkyS03)
{
	mapTo = "rooftiles.SkyS05";

	diffuseMap[0] = "art/textures/structures/SkyScrapers/SkyS_05.dds";
	normalMap[0] = "";
	specularMap[0] = "";

	diffuseColor[0] = "1 1 1 0";
	specular[0] = "1 1 1 0";
	specularPower[0] = 50;

	doubleSided = false;
	translucent = false;
	translucentBlendOp = "None";
   castShadows = "0";
   materialTag0 = "Chinatown";
   materialTag1 = "Unused?";
};

//--- SkyS_03.dae MATERIALS END ---

//--- SkyS_04.dae MATERIALS BEGIN ---


//--- SkyS_04.dae MATERIALS END ---

//--- SkyS_05.dae MATERIALS BEGIN ---
singleton Material(SkyS_05_rooftiles_SkyS05)
{
	mapTo = "unmapped_mat";

	diffuseMap[0] = "art/textures/structures/SkyScrapers/SkyS_05.dds";
	normalMap[0] = "";
	specularMap[0] = "";

	diffuseColor[0] = "1 1 1 0";
	specular[0] = "1 1 1 0";
	specularPower[0] = 50;

	doubleSided = false;
	translucent = false;
	translucentBlendOp = "None";
   castShadows = "0";
   materialTag0 = "Chinatown";
   materialTag1 = "Unused?";
};

//--- SkyS_05.dae MATERIALS END ---

//--- SkyS_06.dae MATERIALS BEGIN ---
singleton Material(SkyS_06_rooftiles_SkyS06)
{
	mapTo = "rooftiles.SkyS04";

	diffuseMap[0] = "art/textures/structures/SkyScrapers/SkyS_05.dds";
	normalMap[0] = "";
	specularMap[0] = "";

	diffuseColor[0] = "1 1 1 0";
	specular[0] = "0 0 0 0";
	specularPower[0] = 1;

	doubleSided = false;
	translucent = false;
	translucentBlendOp = "None";
	materialTag0 = "Chinatown";
	materialTag1 = "Unused?";
};

//--- SkyS_06.dae MATERIALS END ---

//--- SkyS_07.dae MATERIALS BEGIN ---
singleton Material(SkyS_07_rooftiles_SkyS07)
{
	mapTo = "rooftiles.SkyS07";

	diffuseMap[0] = "art/textures/structures/SkyScrapers/SkyS_07.dds";
	normalMap[0] = "";
	specularMap[0] = "";

	diffuseColor[0] = "1 1 1 0";
	specular[0] = "1 1 1 0";
	specularPower[0] = 50;

	doubleSided = false;
	translucent = false;
	translucentBlendOp = "None";
	materialTag0 = "Chinatown";
	materialTag1 = "Unused?";
};

//--- SkyS_07.dae MATERIALS END ---




singleton Material(SkyS_01)
{
   mapTo = "unmapped_mat";
   diffuseMap[0] = "art/textures/structures/SkyScrapers/SkyS_06.dds";
   specular[0] = "1 1 1 0";
   specularPower[0] = "1";
   translucentBlendOp = "None";
   materialTag0 = "Chinatown";
   materialTag1 = "Unused?";
};

singleton Material(SkyS_02_rooftiles_SkyS02)
{
   mapTo = "unmapped_mat";
   diffuseMap[0] = "art/textures/structures/SkyScrapers/SkyS_06.dds";
   specular[0] = "1 1 1 0";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   materialTag0 = "Chinatown";
   materialTag1 = "Unused?";
};

singleton Material(SkyS_04_rooftiles_SkyS04)
{
   mapTo = "unmapped_mat";
   diffuseMap[0] = "art/textures/structures/SkyScrapers/SkyS_04.dds";
   specular[0] = "1 1 1 0";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   materialTag0 = "Chinatown";
   materialTag1 = "Unused?";
};

singleton Material(SkyS_05_rooftiles_SkyS05)
{
   mapTo = "unmapped_mat";
   diffuseMap[0] = "art/textures/structures/SkyScrapers/SkyS_06.dds";
   specular[0] = "1 1 1 0";
   specularPower[0] = "50";
   castShadows = "0";
   translucentBlendOp = "None";
   materialTag0 = "Chinatown";
   materialTag1 = "Unused?";
};

singleton Material(SkyS_03_rooftiles_SkyS03)
{
   mapTo = "rooftiles.SkyS05";
   diffuseMap[0] = "art/textures/structures/SkyScrapers/SkyS_05.dds";
   specular[0] = "1 1 1 0";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   materialTag0 = "Chinatown";
   materialTag1 = "Unused?";
};

singleton Material(SkyS_06_rooftiles_SkyS06)
{
   mapTo = "rooftiles.SkyS04";
   diffuseMap[0] = "art/textures/structures/SkyScrapers/SkyS_05.dds";
   specular[0] = "0 0 0 0";
   specularPower[0] = "1";
   translucentBlendOp = "None";
   materialTag0 = "Chinatown";
   materialTag1 = "Unused?";
};


singleton Material(SkyS_Night_4)
{
   mapTo = "rooftiles.SkyS01";
   diffuseMap[0] = "art/textures/structures/SkyScrapers/SkyS_05.dds";
   materialTag0 = "Chinatown";
   materialTag1 = "SkySNight";
   materialTag1 = "Unused?";
};
//--- Skyline.dae MATERIALS BEGIN ---
singleton Material(Skyline_Skyline)
{
	mapTo = "Skyline";

	diffuseMap[0] = "art/textures/structures/SkyScrapers/SkyLine.dds";
	normalMap[0] = "";
	specularMap[0] = "";

	diffuseColor[0] = "0.7 0.7 0.7 0";
	specular[0] = "1 1 1 0";
	specularPower[0] = 50;

	doubleSided = false;
	translucent = 0;
	translucentBlendOp = "LerpAlpha";
   emissive[0] = "0";
   castShadows = "0";
   translucentZWrite = "0";
   alphaRef = "20";
   alphaTest = "1";
   materialTag0 = "Chinatown";
   materialTag1 = "SkySNight";
   materialTag1 = "Unused?";
};

//--- Skyline.dae MATERIALS END ---

